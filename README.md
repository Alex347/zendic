# ZenDic

**Détails du Projet**

Faire une application mobile, adaptable aux navigateurs et éventuellement applicative Desktop.
Le sujet est de faire une interface communicative et d'échanges pour des résidences sans gardien;
Elle ferait le lien entre les locataires, les co-propriétaires, les propriétaires et le syndicat.

**Ressources**

[Trello](https://trello.com/b/JSONdnlr/zendic-communication-de-copro)
[Ionic Documentation](https://ionicframework.com/docs)
[Symfony Documentation](https://symfony.com/doc/current/index.html#gsc.tab=0)

**INSTALLATION**

DataBase SQL
NodeJS Installation
Android Studio Installation Instructions

[SYMFONY](https://symfony.com/doc/current/setup.html)

IONIC:

Install Ionic:npm install -g ionic cordova

Lancer appli et construire en .apk: ionic cordova run android -l

